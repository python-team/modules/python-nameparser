Source: python-nameparser
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Edward Betts <edward@4angle.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-dill,
               python3-setuptools
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/derek73/python-nameparser
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-nameparser
Vcs-Git: https://salsa.debian.org/python-team/packages/python-nameparser.git

Package: python3-nameparser
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Python 3 module for parsing names into individual components
 The HumanName class splits a name string up into name parts based on placement
 in the string and matches against known name pieces like titles. It joins name
 pieces on conjunctions and special prefixes to last names like "del". Titles
 can be chained together and include conjunctions to handle titles like
 "Asst Secretary of State". It can also try to correct capitalization of all
 upper or lowercase names.
 .
 It attempts the best guess that can be made with a simple, rule-based approach.
 Unicode is supported, but the parser is not likely to be useful for languages
 that to not share the same structure as English names. It's not perfect, but it
 gets you pretty far.
